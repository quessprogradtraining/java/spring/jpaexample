package com.example.demo8.service;

import com.example.demo8.model.Division;
import org.springframework.stereotype.Service;

@Service
public class DivisionService {
    public int division(Division divisionObj){
        int result = 0;
        try {
             result = divisionObj.getNum1() / 0;

            divisionObj.setResult(result);
            
        }
        catch (Exception exception){
            System.out.println("Exception Handled");
        }
        return result;
    }
}
