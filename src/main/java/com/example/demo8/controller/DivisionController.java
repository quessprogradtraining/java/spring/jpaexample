package com.example.demo8.controller;

import com.example.demo8.model.Division;
import com.example.demo8.service.DivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DivisionController {
    @Autowired
    DivisionService divisionServiceObj;
    @GetMapping("/check")
    public String check(){
        Division divisionObj=new Division(10,5);
        int result=divisionServiceObj.division(divisionObj);

        return"division is:"+result;

    }

}
