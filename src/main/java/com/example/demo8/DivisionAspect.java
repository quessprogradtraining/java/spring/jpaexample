package com.example.demo8;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class DivisionAspect {
    @Before("execution(* com.example.demo8.controller.DivisionController.check())")
    public  void beforedivision(){
        System.out.println("division process started");
    }
    @After("execution(* com.example.demo8.controller.DivisionController.check())")
    public void afterDivision(){
        System.out.println("division process completed");
    }

}
